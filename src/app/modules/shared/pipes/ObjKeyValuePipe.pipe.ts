import {Pipe, PipeTransform} from '@angular/core';
import {KeyValue} from '@angular/common';

@Pipe({
  name: 'entry'
})
export class EntryPipe implements PipeTransform {

  transform<K extends (number | string), V>(input?: Record<K, V>): Array<KeyValue<string, V>> {
    return Object.entries(input || {}).map(([key, value]) => ({key, value}) as KeyValue<string, V>);
  }
}
