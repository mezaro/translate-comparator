import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Clipboard} from '@angular/cdk/clipboard';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CopyTemplateComponent} from '../copy-template/copy-template.component';
import {Translation} from '../../../../interfaces';
import {TranslationService} from '../../../../services/translation.service';

@Component({
  selector: 'app-json-dialog',
  templateUrl: './json-dialog.component.html',
  styleUrls: ['./json-dialog.component.scss']
})
export class JsonDialogComponent {
  json: string;
  preview: { key: string, value: string }[];

  constructor(
    public ref: MatDialogRef<JsonDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Translation,
    private clipboard: Clipboard,
    private snackBar: MatSnackBar,
    private translationService: TranslationService,
  ) {
    this.preview = Object.entries(this.data).map(([key, values]) => ({key, value: values.value}));
    this.json = JSON.stringify(this.translationService.separateKeys(this.data), null, 2);
  }

  onCopy(): void {
    this.clipboard.copy(this.json);
    this.snackBar.openFromComponent(CopyTemplateComponent, {duration: 1500});
  }

  onSave(): void {
    const a: HTMLAnchorElement = document.createElement('a');
    const file = new Blob([this.json], {type: 'text/json'});
    a.href = URL.createObjectURL(file);
    a.download = 'translations.json';
    a.click();
  }
}
