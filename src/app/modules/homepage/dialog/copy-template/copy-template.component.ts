import {Component} from '@angular/core';

@Component({
  template: '<p class="center" translate="SAVED"></p>',
})
export class CopyTemplateComponent {
}
