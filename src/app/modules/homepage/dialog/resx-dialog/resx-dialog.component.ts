import {Component, Inject} from '@angular/core';
import {CopyTemplateComponent} from '../copy-template/copy-template.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Translation} from '../../../../interfaces';
import {Clipboard} from '@angular/cdk/clipboard';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DOCUMENT} from '@angular/common';
import {spaces} from '../../../../functions';
import {after, before} from './res-base';

@Component({
  selector: 'app-resx-dialog',
  templateUrl: './resx-dialog.component.html',
  styleUrls: ['./resx-dialog.component.scss']
})
export class ResxDialogComponent {
  resx!: string;

  constructor(
    public ref: MatDialogRef<ResxDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Translation,
    private clipboard: Clipboard,
    private snackBar: MatSnackBar,
    @Inject(DOCUMENT) private document: Document,
  ) {
    this.createResx();
  }

  createResx(): void {
    this.resx = before;
    Object.entries(this.data).forEach(([key, values]) => {
      const data = this.createNode('data', {'xml:space': 'preserve', name: key});
      const value = this.createNode('value');
      value.textContent = values.value;
      data.append(value);
      if (values.comment) {
        const comment = this.createNode('comment');
        comment.textContent = values.comment;
        data.append(comment);
      }
      this.resx += this.serializeNode(data);
    });
    this.resx += after;
  }

  serializeNode(node: HTMLElement): string {
    let xml = node.outerHTML;
    xml = xml.replace(/(<data(.*)">)/gm, s => `${spaces(2)}${s}\n`);
    xml = xml.replace(/<value(.*?)>|<comment(.*?)>/gm, s => `${spaces(4)}${s}`);
    xml = xml.replace(/<\/value(.*?)>|<\/comment(.*?)>/gm, s => `${s}\n`);
    xml = xml.replace(/(<\/data>)/gm, s => `${spaces(2)}${s}\n`);
    return xml;
  }

  createNode(name: string, attributes?: Record<string, string>): HTMLElement {
    const node = this.document.createElement(name);
    Object.entries(attributes || {}).forEach(([attrName, attrValue]) => {
      node.setAttribute(attrName, attrValue);
    });
    return node;
  }

  onCopy(): void {
    this.clipboard.copy(this.resx);
    this.snackBar.openFromComponent(CopyTemplateComponent, {duration: 1500});
  }

  onSave(): void {
    const a: HTMLAnchorElement = document.createElement('a');
    const file = new Blob([this.resx], {type: 'text/xml'});
    a.href = URL.createObjectURL(file);
    a.download = 'translations.resx';
    a.click();
  }
}
