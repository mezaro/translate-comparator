import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Translation, TranslationChange} from '../../../../interfaces';
import {ChangeTypeColor, ChangeTypeIndicator} from '../../../../enums';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreviewComponent {
  @Input() translationChanges?: TranslationChange[];
  @Input() translation?: Translation;
  colors = ChangeTypeColor;
  indicators = ChangeTypeIndicator;
}
