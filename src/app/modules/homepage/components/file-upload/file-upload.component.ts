import {Component, EventEmitter, Output} from '@angular/core';
import {UploadedFile, FileType} from '../../../../interfaces';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {
  @Output() upload = new EventEmitter<UploadedFile>();
  file?: File | null;
  form = new FormControl('');

  onSelect(input: HTMLInputElement): void {
    if (this.file) {
      this.form.reset();
    }
    input.click();
  }

  onFileChange(event: Event): void {
    const file = (event.target as HTMLInputElement).files?.item(0);
    let type: FileType | undefined;
    if (file?.name.includes('.json')) {
      type = 'json';
    }
    if (file?.name.includes('.resx')) {
      type = 'resx';
    }

    if (file && type) {
      const reader = new FileReader();
      reader.readAsText(file, 'UTF-8');
      reader.onload = (e) => {
        this.file = file;
        this.upload.emit({type: type as FileType, content: e.target?.result as string});
      };
    }
  }
}
