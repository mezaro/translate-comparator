import {NgModule} from '@angular/core';
import {HomepageRoutingModule} from './homepage-routing.module';
import {HomepageComponent} from './homepage.component';
import {HeaderComponent} from './components/header/header.component';
import {FileUploadComponent} from './components/file-upload/file-upload.component';
import {SharedModule} from '../shared/shared.module';
import {PreviewComponent} from './components/preview/preview.component';
import {JsonDialogComponent} from './dialog/json-dialog/json-dialog.component';
import {ResxDialogComponent} from './dialog/resx-dialog/resx-dialog.component';
import {CopyTemplateComponent} from './dialog/copy-template/copy-template.component';


@NgModule({
  declarations: [
    HomepageComponent,
    HeaderComponent,
    FileUploadComponent,
    PreviewComponent,
    JsonDialogComponent,
    ResxDialogComponent,
    CopyTemplateComponent
  ],
  imports: [
    SharedModule,
    HomepageRoutingModule,
  ]
})
export class HomepageModule {
}
