import {Component, OnDestroy} from '@angular/core';
import {FileType, Translation, TranslationChange, TranslationStatistics, UploadedFile} from '../../interfaces';
import {TranslationService} from '../../services/translation.service';
import {ChangeType} from '../../enums';
import {MatDialog} from '@angular/material/dialog';
import {JsonDialogComponent} from './dialog/json-dialog/json-dialog.component';
import {ResxDialogComponent} from './dialog/resx-dialog/resx-dialog.component';
import {ComponentType} from '@angular/cdk/overlay';
import {FormControl, FormGroup} from '@angular/forms';
import {SubscriptionLike} from 'rxjs';

type OperationFileType = 'CURRENT' | 'NEW';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnDestroy {
  versionOneTranslations?: Translation;
  versionTwoTranslations?: Translation;

  translationChanges?: TranslationChange[];
  filteredTranslationChanges?: TranslationChange[];

  statistics?: TranslationStatistics;
  filters: FormGroup = new FormGroup({
    LACK: new FormControl(true),
    INSERT: new FormControl(true),
    UPDATE: new FormControl(true),
    DELETE: new FormControl(true),
  } as Record<keyof typeof ChangeType, FormControl>);

  duplicatesOnly = false;
  duplicates?: number;
  filteredDuplicates?: Translation;

  get lackInput(): FormControl {
    return this.filters.controls.LACK as FormControl;
  }

  get insertInput(): FormControl {
    return this.filters.controls.INSERT as FormControl;
  }

  get updateInput(): FormControl {
    return this.filters.controls.UPDATE as FormControl;
  }

  get deleteInput(): FormControl {
    return this.filters.controls.DELETE as FormControl;
  }

  private subscription?: SubscriptionLike;

  constructor(
    private translationService: TranslationService,
    private dialog: MatDialog,
  ) {
    this.initFilters();
  }

  initFilters(): void {
    this.subscription = this.filters.valueChanges.subscribe((filters: Record<keyof typeof ChangeType, boolean>) => {
      this.filteredTranslationChanges = this.translationChanges?.filter(t => filters[t.type]);
    });
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  onVersionOneUpload(file: UploadedFile): void {
    this.parseInputFile(file, 'CURRENT');
    this.detectDuplicates();
    this.detectChanges();
  }

  onVersionTwoUpload(file: UploadedFile): void {
    this.parseInputFile(file, 'NEW');
    this.detectChanges();
  }

  private parseInputFile(file: UploadedFile, type: OperationFileType): void {
    switch (file.type) {
      case 'json':
        this.parseJsonFile(file, type);
        break;
      case 'resx':
        this.parseResXFile(file, type);
        break;
    }
  }

  private parseJsonFile(file: UploadedFile, type: OperationFileType): void {
    switch (type) {
      case 'CURRENT':
        this.versionOneTranslations = this.translationService.parseJson(file);
        break;
      case 'NEW':
        this.versionTwoTranslations = this.translationService.parseJson(file);
        break;
    }
  }

  private parseResXFile(file: UploadedFile, type: OperationFileType): void {
    switch (type) {
      case 'CURRENT':
        this.versionOneTranslations = this.translationService.parseResX(file);
        break;
      case 'NEW':
        this.versionTwoTranslations = this.translationService.parseResX(file);
        break;
    }
  }

  detectDuplicates(): void {
    const counts: Record<string, { count: number; key: string }> = {};
    const translations = Object.entries(this.versionOneTranslations || {});

    translations.forEach(([key, values]) => {
      const obj = counts[values.value];
      if (obj === undefined) {
        counts[values.value] = {key, count: 1};
      } else {
        obj.count++;
      }
    });

    translations.forEach(([key, values]) => {
      // tslint:disable-next-line:no-non-null-assertion
      this.versionOneTranslations![key].duplicates = counts[values.value].count;
    });

    this.duplicates = translations.filter(([_, values]) => values.duplicates && values.duplicates > 1).length;
    this.filteredDuplicates = JSON.parse(JSON.stringify(this.versionOneTranslations));
    if (this.duplicatesOnly) {
      translations.forEach(([key, value]) => {
        if (!value.duplicates || value.duplicates < 2) {
          // tslint:disable-next-line:no-non-null-assertion
          delete this.filteredDuplicates!![key];
        }
      });
    }
  }

  detectChanges(): void {
    const filters: Record<keyof typeof ChangeType, boolean> = this.filters.getRawValue();
    this.translationChanges = this.translationService.detectChanges(this.versionOneTranslations, this.versionTwoTranslations);
    this.filteredTranslationChanges = this.translationChanges?.filter(t => filters[t.type]);
    this.statistics = this.translationService.generateStatistics(this.translationChanges);
  }

  onExportJson(): void {
    this.onExport('json');
  }

  onExportResx(): void {
    this.onExport('resx');
  }

  onExport(type: FileType): void {
    if (this.translationChanges) {
      this.openDialog(this.translationChanges
          .filter(t => t.type !== ChangeType.DELETE)
          .reduce((jsonObj, translation) => {
            jsonObj[translation.key] = {value: translation.new, comment: translation.comment};
            return jsonObj;
          }, {} as Translation)
        , type);
    } else if (this.versionOneTranslations) {
      this.openDialog(this.versionOneTranslations, type);
    }
  }

  openDialog(data: Translation, type: FileType): void {
    const dialogs: { [key in FileType]: ComponentType<unknown> } = {
      json: JsonDialogComponent,
      resx: ResxDialogComponent
    };
    this.dialog.open(dialogs[type], {data});
  }

  onDuplicatesFilter(): void {
    this.duplicatesOnly = !this.duplicatesOnly;
    this.detectDuplicates();
  }
}
