export const TOKENS = 'tokens';

export interface Tokens {
  accessToken?: AccessToken;
  refreshToken?: RefreshToken;
}

export type AccessToken = string;
export type RefreshToken = string;
