import {ChangeType} from '../../enums';

export interface TranslationChange {
  type: ChangeType;
  key: string;
  current: string;
  new: string;
  comment?: string;
}

export type TranslationStatistics = Record<keyof typeof ChangeType, number>;
