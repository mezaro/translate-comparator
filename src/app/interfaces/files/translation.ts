export type Translation = Record<string | number, TranslationValues>;

export interface TranslationValues {
  value: string;
  comment?: string;
  duplicates?: number;
}
