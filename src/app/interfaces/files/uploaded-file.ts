export interface UploadedFile {
  type: FileType;
  content: string;
}

export type FileType = 'json' | 'resx';
