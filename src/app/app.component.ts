import {Component, HostListener} from '@angular/core';
import {ApiService} from './services/api.service';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
  constructor(
    private api: ApiService
  ) {
  }

  @HostListener('window:unload')
  onClose(): void {
    this.closeServer();
    const date = new Date().getTime() + 100;
    while (new Date().getTime() < date) {
    }
  }

  async closeServer(): Promise<void> {
    try {
      await this.api.close().toPromise();
    } catch {
    }
  }
}
