import {NgModule} from '@angular/core';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../../environments/environment';
import {StoreModule as NgStoreModule} from '@ngrx/store';


@NgModule({
  imports: [
    NgStoreModule.forRoot({}, {}),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
  ],
  exports: [
    NgStoreModule,
    StoreDevtoolsModule,
  ]
})
export class StoreModule {
}
