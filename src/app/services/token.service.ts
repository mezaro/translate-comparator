import { Injectable } from '@angular/core';
import {TOKENS, Tokens} from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private tokens: Tokens;

  constructor() {
    this.tokens = JSON.parse(localStorage.getItem(TOKENS) || '{}') as Tokens;
  }

  saveTokens(tokens: Tokens): void {
    this.tokens = tokens;
    localStorage.setItem(TOKENS, JSON.stringify(tokens));
  }

  clearTokens(): void {
    this.tokens = {};
    localStorage.removeItem(TOKENS);
  }

  getTokens(): Tokens {
    return this.tokens;
  }
}
