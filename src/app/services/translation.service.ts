import {Injectable} from '@angular/core';
import {Translation, TranslationChange, TranslationStatistics, UploadedFile} from '../interfaces';
import {ChangeType} from '../enums';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  constructor() {
  }

  parseJson(file: UploadedFile): Translation {
    try {
      return this.joinKeys(JSON.parse(file.content) || {});
    } catch {
      return {};
    }
  }

  parseResX(file: UploadedFile): Translation {
    const translations: Translation = {};
    new DOMParser()
      .parseFromString(file.content, 'application/xml')
      .querySelectorAll('data[name]')
      .forEach((data) => {
        const key = data.getAttribute('name');
        if (key) {
          const value = data.querySelector('value')?.textContent || '';
          const comment = data.querySelector('comment')?.textContent;
          if (value && comment) {
            translations[key] = {value, comment};
          } else {
            translations[key] = {value};
          }
        }
      });
    return translations;
  }

  joinKeys(translation: Record<string | number, any>, dist: Translation = {}, parents: string[] = []): Translation {
    Object.entries(translation).forEach(([key, value]) => {
      if (typeof value === 'object') {
        this.joinKeys(value, dist, [...parents, key]);
      } else {
        dist[[...parents, key].join('.')] = {value: `${value}`};
      }
    });
    return dist;
  }

  separateKeys(translation: Translation): Record<string | number, any> {
    const dist: Record<string | number, any> = {};
    Object.entries(translation).forEach(([key, value]) => {
      key.split('.').reduce((obj, subKey, i, arr) => {
        if ((i + 1) === arr.length) {
          obj[subKey] = value.value;
        } else if (!obj[subKey]) {
          obj[subKey] = {};
        }
        return obj[subKey];
      }, dist);
    });
    return dist;
  }

  parseTranslationToJson(translation: Translation): string {
    return JSON.stringify(translation, null, 2);
  }

  generateStatistics(translationChanges?: TranslationChange[]): TranslationStatistics | undefined {
    if (!translationChanges) {
      return undefined;
    }
    const stats: TranslationStatistics = {DELETE: 0, UPDATE: 0, LACK: 0, INSERT: 0};
    translationChanges.forEach(({type}) => {
      stats[type]++;
    });
    return stats;
  }

  detectChanges(currentTranslations?: Translation, newTranslations?: Translation): TranslationChange[] | undefined {
    if (currentTranslations && newTranslations) {
      const currentClone: Translation = JSON.parse(JSON.stringify(currentTranslations));
      const newClone: Translation = JSON.parse(JSON.stringify(newTranslations));
      const translationChanges: TranslationChange[] = [];
      // Detect changes or remove
      Object.entries(currentClone).forEach(([key, value]) => {
        const newTranslation = newClone[key];
        if (newTranslation) {
          const type = value.value === newTranslation.value ? ChangeType.LACK : ChangeType.UPDATE;
          translationChanges.push({
            key, type,
            current: value.value,
            new: newTranslation.value,
            comment: value.comment,
          });
          delete newClone[key];
        } else {
          translationChanges.push({
            key,
            type: ChangeType.DELETE,
            current: value.value,
            new: '- -',
            comment: value.comment,
          });
        }
      });
      // Push inserts
      Object.entries(newClone).forEach(([key, value]) => {
        translationChanges.push({
          key,
          type: ChangeType.INSERT,
          current: '- -',
          new: value.value,
          comment: value.comment,
        });
      });
      return translationChanges.sort((a, b) => a.key.localeCompare(b.key));
    }
    return undefined;
  }
}
