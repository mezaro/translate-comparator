import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from '../interfaces';
import {HttpClient} from '@angular/common/http';
import {finalize, shareReplay} from 'rxjs/operators';
import {LoginCredentials, RegisterCredentials} from '../interfaces';
import {TokenService} from './token.service';
import {url} from '../functions';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user$?: Observable<User>;

  constructor(
    private router: Router,
    private http: HttpClient,
    private tokenService: TokenService,
  ) {
  }

  login(credentials: LoginCredentials): Observable<void> {
    return this.http.post<void>(url(`/auth/login`), credentials);
  }

  logout(): Observable<void> {
    return this.http.post<void>(url(`/auth/logout`), this.tokenService.getTokens()).pipe(finalize(() => {
      delete this.user$;
      this.tokenService.clearTokens();
    }));
  }

  register(credentials: RegisterCredentials): Observable<void> {
    return this.http.post<void>(url(`/auth/register`), credentials);
  }

  user(): Observable<User> {
    if (!this.user$) {
      this.user$ = this.http.get<User>(url(`/auth/about`)).pipe(shareReplay(1));
    }
    return this.user$;
  }
}
