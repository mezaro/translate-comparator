import {environment} from '../../environments/environment';

export const url = (uri: string) => `${environment.API_URL}${uri}`;

export const spaces = (size: number): string => new Array(size).fill(' ').join('');
