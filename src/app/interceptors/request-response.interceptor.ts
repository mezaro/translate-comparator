import {Injectable} from '@angular/core';
import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import snakecaseKeys from 'snakecase-keys';
import {map} from 'rxjs/operators';
import * as camelcaseKeys from 'camelcase-keys';

@Injectable()
export class RequestResponseInterceptor implements HttpInterceptor {

  private static mapRequest(req: HttpRequest<unknown>): HttpRequest<unknown> {
    return req.clone({
      body: !!req.body && (typeof req.body === 'object' || Array.isArray(req.body))
        ? snakecaseKeys(req.body as any, {deep: true})
        : req.body
    });
  }

  private static mapResponse(res: HttpEvent<unknown>): HttpEvent<unknown> {
    if (res.type === HttpEventType.Response) {
      return res.clone({
        body: !!res.body && (typeof res.body === 'object' || Array.isArray(res.body))
          ? camelcaseKeys(res.body as any, {deep: true})
          : res.body
      });
    }
    return res;
  }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next
      .handle(RequestResponseInterceptor.mapRequest(req))
      .pipe(map(res => RequestResponseInterceptor.mapResponse(res)));
  }
}
