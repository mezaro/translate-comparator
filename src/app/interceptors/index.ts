import {AuthInterceptor} from './auth.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {RequestResponseInterceptor} from './request-response.interceptor';

const HttpAuthInterceptor = {
  provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true
};

const HttpRequestResponseInterceptor = {
  provide: HTTP_INTERCEPTORS, useClass: RequestResponseInterceptor, multi: true
};

export const interceptors = [
  HttpAuthInterceptor,
  HttpRequestResponseInterceptor,
];

