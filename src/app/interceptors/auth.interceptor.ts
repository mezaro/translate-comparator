import {Injectable} from '@angular/core';
import {
  HttpBackend,
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import {EMPTY, Observable, of} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {TokenService} from '../services/token.service';
import {Tokens} from '../interfaces';
import {url} from '../functions';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private tokenService: TokenService,
    private router: Router,
    private httpBackend: HttpBackend,
  ) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(this.applyToken(request)).pipe(
      catchError((requestError) => {
        if (requestError instanceof HttpErrorResponse) {
          switch (requestError.status) {
            case 401:
              return this.refresh().pipe(
                switchMap(tokens => {
                  this.tokenService.saveTokens(tokens);
                  return next.handle(this.applyToken(request));
                }),
                catchError(() => {
                  this.tokenService.clearTokens();
                  this.router.navigate(['/auth/login']).catch();
                  return EMPTY;
                })
              );
            case 403:
              this.router.navigateByUrl('/403').catch();
              return EMPTY;
            case 500:
              this.router.navigateByUrl('/500').catch();
              return EMPTY;
          }
        }
        return of(requestError);
      })
    );
  }

  private refresh(): Observable<Tokens> {
    return new HttpClient(this.httpBackend).post<Tokens>(url(`/auth/token`), this.tokenService.getTokens());
  }

  private applyToken(request: HttpRequest<unknown>): HttpRequest<unknown> {
    return !!this.tokenService.getTokens().accessToken ?
      request.clone({headers: request.headers.set('Authorization', `Bearer ${this.tokenService.getTokens().accessToken}`)}) : request;
  }
}
