export enum ChangeType {
  LACK = 'LACK',
  INSERT = 'INSERT',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
}

export enum ChangeTypeColor {
  LACK = 'color-gray-4',
  INSERT = 'color-secondary-4 bold',
  UPDATE = 'color-secondary-1 bold',
  DELETE = 'color-error-1 bold',
}

export enum ChangeTypeIndicator {
  LACK = 'background-gray-4',
  INSERT = 'background-secondary-4',
  UPDATE = 'background-secondary-1',
  DELETE = 'background-error-1',
}
