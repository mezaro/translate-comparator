import * as path from 'path';
import * as express from 'express';
import {Server} from 'http';
import * as open from 'open';

const app = express();
const port = 8090;
let server: Server;

function log(message: string): void {
  console.log('\x1b[32m%s \x1b[37m%s', `[INFO]`, `[${new Date().toJSON()}] ${message}`);
}

log('Starting app.');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/close', (req, res) => {
  res.status(200).send();
  log(`Server closed.`);
  server?.close();
  process.exit(1);
});

server = app.listen(port, async () => {
  log(`App is running on port ${port}`);
  log(`Opening browser.`);
  try {
    await open(`http://127.0.0.1:${port}`);
  } catch {
    log('Opening browser Failed. Open manually: http://127.0.0.1:${port}');
  }
  log(`Press CTRL + C to close app.`);
});
